import logo from './logo.svg'
import './App.css'
import CategoryPage from './pages/categoryPage'
import ProductPage from './pages/productPage'

function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <CategoryPage />
        </div>
        <div className="col">
          <ProductPage />
        </div>
      </div>
    </div>
  )
}

export default App
